# Les templates intelliJ

La creation de documentation pour les Getter, Setter, les entête de classes peut être documenter et voici comment :  

## Pour les Getter setter

### Accéder au menu de generation de code
vous aurez besoin d'une classe avec des getter et des setter a faire pour cette partie

1. Pour accèder aux raccourcis le alt + insert et passer par "Getter and Setter" ou rapidement peser deux fois sur shift et écrire Setter
2. une fois dans le raccourci du Setter, vous allez pesez sur les trois petit point 
3. ![Menu](Menu_Template.png)
4. Pour que le nouveau Template de genération de code fonctionne vous devez dupliquer le template par défault d'IntelliJ
5. Selectionnez le template d'IntelliJ et cliquez sur les deux feuille verte en haut a gauche
6. ![Duplicate](./Duplicate.png)
7. Donnez un nom a votre Template
8. À partir de ce template vous pouvez maintenant configurez le code qu'IntelliJ vous génère lors de la creation automatique d'un Setter
9. Pour faire le documentation automatique de ces bout de code vous n'avez qu'a rajouter au dessus de tout : 
```java
/** 
 * Setter pour $field.name 
 * 
 * @param $field.name $field.type du $field.name
**/ 
```
10. pour le Getter faite les même étape, mais remplacer Setter pour Getter

## Pour les entêtes de classes

### Concerne principalement les étudiants en deuxième session et plus 
1. Allez dans les paramêtres de génération de fichers Java (Settings -> Editor -> File and code Templates -> (**Class**, Interface ou Enum))
2. Ces configuration gère ce qui créé par IntelliJ lors de la création d'un nouveaux fichier Java
3. Pour Générer de la documentation sur toute les classes, vous qu'a ajouter : 
```java
    /**
     * Contient les informations sur ${NAME}
     * 
     * @author $USER
     * @version 1.0.0, $DATE
     */
```
4. Entre `#Parse...` et `public class ${NAME}...`
5. ![Class](Class.png)
6. $USER peut être remplacer par un nom plus descriptif si $USER n'est pas adéquat


**Ce document n'est pas complet, les template ne sont pas restraint a seulement cela, vous êtes encouragez a explorez de votre coté** <br>
**Je n'ai pas abordé les autres partie de la syntaxe des Templates, car elle est complexe, mais vous pouvez sois la déduire par vous même ou allez voire "Apache Velocity" la syntaxe est supposé être basé sur ça**